package org.example;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TransportFactoryTest {
    City moscow = new City("Moscow", 240, true, false);
    City tula = new City("Tula", 714, false, false);
    City stPetersburg = new City("St.Petersburg", 1080, true, true);
    @Test
    public void CapacityRoundingShouldBeMultipleOf500() {
        assertEquals(500, TransportFactory.getTransport(moscow, 346, 5).getCapacity());
        assertEquals(500, TransportFactory.getTransport(moscow, 500, 5).getCapacity());
        assertEquals(1000, TransportFactory.getTransport(moscow, 567, 5).getCapacity());
        assertEquals(1000, TransportFactory.getTransport(moscow, 1000, 5).getCapacity());
        assertEquals(4500, TransportFactory.getTransport(moscow, 4001, 5).getCapacity());
        assertEquals(4500, TransportFactory.getTransport(moscow, 4500, 5).getCapacity());
}
    @Test
    public void SpeedRoundingShouldBeMultipleOf10(){
        assertEquals(20, TransportFactory.getTransport(moscow, 600, 15).getSpeed());
        assertEquals(20, TransportFactory.getTransport(moscow, 600, 12).getSpeed());
        assertEquals(360, TransportFactory.getTransport(stPetersburg, 600, 3).getSpeed());
        assertEquals(360, TransportFactory.getTransport(tula, 600, 2).getSpeed());
    }
    @Test
    public void TransportShouldBePlane(){
        assertEquals(Plane.class, TransportFactory.getTransport(stPetersburg, 600, 3).getClass());
        assertNotEquals(Plane.class, TransportFactory.getTransport(tula, 600, 2).getClass());
        assertNotEquals(Plane.class, TransportFactory.getTransport(moscow, 600, 10).getClass());
    }
    @Test
    public void TransportShouldBeTruck(){
        assertEquals(Truck.class, TransportFactory.getTransport(tula, 600, 1).getClass());
        assertEquals(Truck.class, TransportFactory.getTransport(tula, 600, 7).getClass());
        assertEquals(Truck.class, TransportFactory.getTransport(stPetersburg, 600, 17).getClass());
    }
    @Test
    public void TransportShouldBeShip(){
        assertEquals(Ship.class, TransportFactory.getTransport(stPetersburg,600, 48).getClass());
        assertNotEquals(Ship.class, TransportFactory.getTransport(stPetersburg,600, 10).getClass());
        assertNotEquals(Ship.class, TransportFactory.getTransport(moscow,600, 48).getClass());
    }
}
