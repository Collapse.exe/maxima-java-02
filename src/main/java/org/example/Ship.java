package org.example;

public class Ship extends Transport {
    public float getPrice (City city) {
        return city.isOnWater ? getCostOfKm() * city.getDistanceKm() : 0;
    }

    public Ship(String name, int capacity, int speed, float costOfKm) {
        super(name, capacity, speed, costOfKm);
    }
}
