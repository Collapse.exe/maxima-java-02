package org.example;

public class Logistics {
    Transport[] vehicles;

    public Logistics(Transport... vehicles) {
        this.vehicles = vehicles;
    }

    public Transport getShipping(City city, int weight, int hours){
        Transport min = null;
        int i = 0;
        while (!isShippingAvailable(city,weight,hours,i) && i < vehicles.length){
            i++;
        }
        if (i < vehicles.length){
            min = vehicles[i];
            while (i < vehicles.length){
                i++;
                if (isShippingAvailable(city,weight,hours,i) && min.getPrice(city) > vehicles[i].getPrice(city)){
                    min = vehicles[i];
                }
            }
        }
        return min;
    }

    private boolean isShippingAvailable (City city, int weight, int hours, int i){
        return i < vehicles.length && vehicles[i].getCapacity() >= weight && vehicles[i].getShippingTime(city) <= hours
                && vehicles[i].getPrice(city) > 0 && !vehicles[i].isRepairing();
    }
}
