package org.example;

final class TransportFactory {

    public static Transport getTransport(City city, int weight, int hours){
        final String NAME_OF_SHIP = "Ship";
        final String NAME_OF_PLANE = "Plane";
        final String NAME_OF_TRUCK = "Truck";
        final float COST_OF_KM_FOR_SHIP = 1.44f;
        final float COST_OF_KM_FOR_PLANE = 4.5f;
        final float COST_OF_KM_FOR_TRUCK = 0.66f;

        int requiredSpeed = city.getDistanceKm() / hours;
        requiredSpeed = requiredSpeed % 10 == 0 ? requiredSpeed : (requiredSpeed / 10 + 1) * 10;
        int requiredCapacity = weight % 500 == 0 ? weight : (weight / 500 + 1) * 500;
        Transport transport;
        if (requiredSpeed <= 40 && city.isOnWater){
            transport = new Ship(NAME_OF_SHIP, requiredCapacity, requiredSpeed, COST_OF_KM_FOR_SHIP);
        }
        else if (requiredSpeed >= 120 && city.hasAirport){
            transport = new Plane(NAME_OF_PLANE, requiredCapacity, requiredSpeed, COST_OF_KM_FOR_PLANE);
        }
        else {
            transport = new Truck(NAME_OF_TRUCK, requiredCapacity, requiredSpeed, COST_OF_KM_FOR_TRUCK);
        }
    return transport;
    }
}
