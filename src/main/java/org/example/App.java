package org.example;

public class App {
    public static void main (String[] args) {
        Ship boat = new Ship("Boat",160,60,1.32f);
        Ship shipDaddy = new Ship("Daddy", 1000,50, 1.78f);
        City moscow = new City("Moscow",240, true, false);
        Plane boing = new Plane("Boing",300,225,4.20f);
        Plane helicopter = new Plane("Helico15", 15, 200, 2.23f);
        Truck kamaz = new Truck("Kamaz",100,70,0.87f);
        Truck jeep = new Truck("Jeep10",10,90,0.5f);
/*
        Logistics toMoscow = new Logistics(boat,shipDaddy,boing,helicopter,kamaz,jeep) {
        };
        System.out.println(kamaz.getPrice(moscow));
        System.out.println(jeep.getPrice(moscow));
        System.out.println(boat.getPrice(moscow));
        System.out.println(boing.getPrice(moscow));
        helicopter.startRepair();
        boing.startRepair();
        System.out.println(toMoscow.getShipping(moscow, 15,2));
        helicopter.finishRepair();
        System.out.println(toMoscow.getShipping(moscow,15,2));
        System.out.println(boing.isRepairing());
        System.out.println(jeep.isRepairing());*/
        System.out.println(TransportFactory.getTransport(moscow, 1000, 1).getCapacity());
        Transport superVehicle = TransportFactory.getTransport(moscow, 659, 4);
        superVehicle.setName("Бэтмобиль");
        System.out.println(superVehicle.getName());

    }
}
